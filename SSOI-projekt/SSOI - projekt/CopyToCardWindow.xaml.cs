﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.VisualBasic;

using GS.PCSC;
using GS.SCard;
using GS.SCard.Const;

namespace SSOI___projekt
{
    /// <summary>
    /// Interaction logic for CopyToCardWindow.xaml
    /// </summary>
    public partial class CopyToCardWindow : Window
    {
        private WinSCard scard;
        private string readerName;
        private int numberOfShadows;
        private int thresholdValue;
        private List<int> x;
        private List<List<byte>> shadows;
        private int currentIndex = 0;

        public CopyToCardWindow(int numberOfShadows, int thresholdValue, List<List<byte>> shadows)
        {
            InitializeComponent();

            this.numberOfShadows = numberOfShadows;
            this.thresholdValue = thresholdValue;
            this.shadows = shadows;

            x = new List<int>();
            for (int i = 1; i <= numberOfShadows; i++)
                x.Add(i);

            ListReaders();
        }

        private void ListReaders()
        {
            if ((currentIndex) == numberOfShadows)
                this.Close();

            this.Title = "Kopiowanie Cieni: " + (currentIndex + 1) + "/" + numberOfShadows;
            
            scard = new WinSCard();
            Select_Reader_ComboBox.Items.Clear();
            try
            {
                scard.EstablishContext();
                scard.ListReaders();

                //Console.WriteLine("Available PC/SC Readers:\n");
                for (int i = 0; i < scard.ReaderNames.Length; i++)
                {
                    Select_Reader_ComboBox.Items.Add(scard.ReaderNames[i]);
                }
                Copy_To_Card_Button.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
                return;
            }
            
        }

        private void Copy_To_Card_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Połączenie z kartą
                scard.WaitForCardPresent(readerName);
                MessageBox.Show("Wykryto kartę");
                scard.Connect(readerName);
                MessageBox.Show("Rozpoczęto kopiowanie sekretu na kartę");

                //Wyswietlenie ATR
                Console.WriteLine("ATR: " + ByteToHex(scard.Atr));

                //Wybranie MF
                Console.WriteLine("\nWybieranie pliku MF");
                byte[] cmdApdu = { 0x00, 0xA4, 0x00, 0x0C, 0x02, 0x3F, 0x00 };
                byte[] respApdu = new byte[256];
                int respLength = respApdu.Length;
                scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);
                if (respApdu[0] != 0x90 || respApdu[1] != 0x00)
                {
                    Console.WriteLine("Response " + ByteToHex(respApdu));
                    MessageBox.Show("Niepowodzenie zapisu do karty Nr: " + (currentIndex + 1), "Błąd");
                    ListReaders();
                    return;
                }
                else
                    Console.WriteLine("Powodzenie");

                //Wybranie DF
                Console.WriteLine("\nWybranie pliku DF 000A");
                cmdApdu = new byte[] { 0x00, 0xA4, 0x01, 0x0C, 0x02, 0x00, 0x0A };
                respApdu = new byte[256];
                respLength = respApdu.Length;
                scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);
                if (respApdu[0] != 0x90 || respApdu[1] != 0x00)
                {
                    Console.WriteLine("Response " + ByteToHex(respApdu));
                    MessageBox.Show("Niepowodzenie zapisu do karty Nr: " + (currentIndex + 1), "Błąd");
                    ListReaders();
                    return;
                }
                else
                    Console.WriteLine("Powodzenie");


                //Wybór pliku transparentnego EF001A
                Console.WriteLine("\nWybór pliku EF001A");
                cmdApdu = new byte[] { 0x00, 0xA4, 0x02, 0x0C, 0x02, 0x00, 0x1A };
                respApdu = new byte[256];
                respLength = respApdu.Length;
                scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);
                if (respApdu[0] != 0x90 || respApdu[1] != 0x00)
                {
                    Console.WriteLine("Response " + ByteToHex(respApdu));
                    MessageBox.Show("Niepowodzenie zapisu do karty Nr: " + (currentIndex + 1), "Błąd");
                    ListReaders();
                    return;
                }
                else
                    Console.WriteLine("Powodzenie");

                //Weryfikacja PIN nr 1
                Console.WriteLine("\nUwierzytelnienie PINem Nr 1");
                string input = Microsoft.VisualBasic.Interaction.InputBox("Podaj PIN", "PIN: ", "");
                byte[] pin = Encoding.ASCII.GetBytes(input);
                List<byte> apduList = new List<byte>();
                apduList.Add(0x00);
                apduList.Add(0x20);
                apduList.Add(0x00);
                apduList.Add(0x81);
                //apduList.Add(0x08);
                apduList.Add((byte)pin.Length);
                apduList.AddRange(pin);
                cmdApdu = apduList.ToArray();
                //cmdApdu = new byte[] { 0x00, 0x20, 0x00, 0x81, 0x08, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31 };
                respApdu = new byte[256];
                respLength = respApdu.Length;
                scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);
                if (respApdu[0] != 0x90 || respApdu[1] != 0x00)
                {
                    Console.WriteLine("Response " + ByteToHex(respApdu));
                    MessageBox.Show("Niepowodzenie zapisu do karty Nr: " + (currentIndex + 1), "Błąd");
                    ListReaders();
                    return;
                }
                else
                    Console.WriteLine("Powodzenie");
                
                //Zapis do pliku transparentnego EF001A
                Console.WriteLine("\nZapis do pliku EF001A");
                int iloscRekordow;
                if ((shadows[currentIndex].Count % 254) == 0)
                    iloscRekordow = shadows[currentIndex].Count / 254;
                else
                    iloscRekordow = (shadows[currentIndex].Count / 254) + 1;
                cmdApdu = new byte[] { 0x00, 0xD6, 0x00, 0x00, 0x04, (byte)numberOfShadows, (byte)thresholdValue, (byte)x[currentIndex], (byte)iloscRekordow };
                respApdu = new byte[256];
                respLength = respApdu.Length;
                scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);
                if (respApdu[0] != 0x90 || respApdu[1] != 0x00)
                {
                    Console.WriteLine("Response " + ByteToHex(respApdu));
                    MessageBox.Show("Niepowodzenie zapisu do karty Nr: " + (currentIndex + 1), "Błąd");
                    ListReaders();
                    return;
                }
                else
                    Console.WriteLine("Powodzenie");

                //Uwierzytelnienie PINem Nr 2
                Console.WriteLine("\nUwierzytelnienie PINem Nr 2");
                input = Microsoft.VisualBasic.Interaction.InputBox("Podaj PIN", "PIN: ", "");
                pin = Encoding.ASCII.GetBytes(input);
                apduList = new List<byte>();
                apduList.Add(0x00);
                apduList.Add(0x20);
                apduList.Add(0x00);
                apduList.Add(0x82);
                //apduList.Add(0x08);
                apduList.Add((byte)pin.Length);
                apduList.AddRange(pin);
                cmdApdu = apduList.ToArray();
                //cmdApdu = new byte[] { 0x00, 0x20, 0x00, 0x82, 0x08, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11 };
                respApdu = new byte[256];
                respLength = respApdu.Length;
                scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);
                if (respApdu[0] != 0x90 || respApdu[1] != 0x00)
                {
                    Console.WriteLine("Response " + ByteToHex(respApdu));
                    MessageBox.Show("Niepowodzenie zapisu do karty Nr: " + (currentIndex + 1), "Błąd");
                    ListReaders();
                    return;
                }
                else
                    Console.WriteLine("Powodzenie");

                //Wybór pliku EF001B
                Console.WriteLine("\nWybór pliku EF001B");
                cmdApdu = new byte[] { 0x00, 0xA4, 0x02, 0x0C, 0x02, 0x00, 0x1B };
                respApdu = new byte[256];
                respLength = respApdu.Length;
                scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);
                if (respApdu[0] != 0x90 || respApdu[1] != 0x00)
                {
                    Console.WriteLine("Response " + ByteToHex(respApdu));
                    MessageBox.Show("Niepowodzenie zapisu do karty Nr: " + (currentIndex + 1), "Błąd");
                    ListReaders();
                    return;
                }
                else
                    Console.WriteLine("Powodzenie");

                //Podział bajtów cieni na rekordy
                List<List<byte>> records = new List<List<byte>>();
                for (int i = 0; i < iloscRekordow; i++)
                    records.Add(new List<byte>());
                int index = 0;
                int value = 1;
                foreach (byte bi in shadows[currentIndex].ToArray())
                {
                    records[index].Add(bi);
                    value++;
                    if (value % 255 == 0)
                    {
                        value = 1;
                        index++;
                    }
                }

                //Zapis do pliku
                Console.WriteLine("\nZapis do pliku EF001B");
                int iloscBajtow = shadows[currentIndex].Count;
                //int index = 0;
                for (int i = 1; i <= iloscRekordow; i++)
                {
                    apduList = new List<byte>();
                    apduList.Add(0x00);
                    apduList.Add(0xDC);
                    apduList.Add((byte)i);
                    apduList.Add(0x04);
                    apduList.Add((byte)records[i - 1].Count);
                    apduList.AddRange(records[i - 1]);

                    cmdApdu = apduList.ToArray();
                    respApdu = new byte[256];
                    respLength = respApdu.Length;
                    Console.WriteLine("Ramka ");
                    foreach (byte bi in cmdApdu)
                        Console.Write(bi + " ");
                    scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);
                    if (respApdu[0] != 0x90 || respApdu[1] != 0x00)
                    {
                        Console.WriteLine("Response " + ByteToHex(respApdu));
                        MessageBox.Show("Niepowodzenie zapisu do karty Nr: " + (currentIndex + 1), "Błąd");
                        ListReaders();
                        return;
                    }
                    else
                        Console.WriteLine("Powodzenie");

                    Console.WriteLine("\nPowodzenia kopiowania cieni na kartę!!!");
                }
                ListReaders();
                currentIndex++;
                //Select_Reader_ComboBox.SelectedIndex = -1;
                //this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd");
            }
            finally
            {
                ListReaders();
            }
        }

        private void Select_Reader_ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                readerName = scard.ReaderNames[Select_Reader_ComboBox.SelectedIndex];
                Copy_To_Card_Button.IsEnabled = true;
            }
            catch (Exception ex) { }
        }

        public string ByteToHex(byte[] array)
        {
            string hex = BitConverter.ToString(array).Replace("-", " ");
            return hex;
        }
    }
}

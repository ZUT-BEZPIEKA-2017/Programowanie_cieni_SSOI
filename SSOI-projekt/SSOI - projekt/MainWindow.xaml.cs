﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Numerics;
using Microsoft.Win32;

namespace SSOI___projekt
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<BigInteger> cienie = new List<BigInteger>();
        BigInteger p = new BigInteger();
        int ilosc_cieni = 5;
        int wartosc_progowa = 3;
        Random rand = new Random();
        public MainWindow()
        {
            InitializeComponent();

          /*
            byte[] fileBytes = openFile();
            BigInteger secret = new BigInteger(fileBytes);
            Console.Write("Sekret " + secret + "\n");
            //BigInteger secret = byteArrayToBigInteger(fileBytes);
            DivideSecretToShadows(secret);

            Console.WriteLine("Ceinie");
            foreach (BigInteger bi in cienie)
                Console.WriteLine(bi);
            Console.WriteLine();

            BigInteger recovery = ShadowsToSecret();
            //Console.WriteLine("BigInt " + recovery);
            byte[] rec = recovery.ToByteArray();
           
            saveFile(rec);
           * */
        }

        //Zapis do pliku
        public void saveFile(byte[] inputBytes)
        {
            //Odwrócenie tablicy bajtów
            byte[] bytesToSave = new byte[inputBytes.Length - 1];
            int currentIndex = 0;
            for (int i = (inputBytes.Length - 2); i >= 0; i--)
            {
                bytesToSave[currentIndex] = inputBytes[i];
                currentIndex++;
            }

            Console.WriteLine("Zapis \n");
            foreach (byte by in inputBytes)
                Console.Write(by.ToString() + " ");
            Console.Write("\n");

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Title = "Zapisz plik";
            //saveFileDialog.Filter = "Tekst jawny (*.src)|*.src|Tekst jawny oczyszczony (*.clr)|*.clr|Tekst zaszyfrowany (*.enc)|*.enc|Tekst odszyfrowany (*.dec)|*.dec";
            saveFileDialog.ShowDialog();

            if (saveFileDialog.FileName != "")
            {
                //string text = Encoding.ASCII.GetString(bytesToSave);
                //System.IO.File.WriteAllText(saveFileDialog.FileName, text);
                System.IO.File.WriteAllBytes(saveFileDialog.FileName, inputBytes);
                
            }
        }

        private void Divide_Secret_Button_Click(object sender, RoutedEventArgs e)
        {
            DivideSecretWindow divideWindow = new DivideSecretWindow();
            divideWindow.Show();
        }

        private void Recovery_Secret_Button_Click(object sender, RoutedEventArgs e)
        {
            RecoveryWindow recoveryWindow = new RecoveryWindow();
            recoveryWindow.Show();
        }

        private void Connect_Button_Click(object sender, RoutedEventArgs e)
        {
            ConnectWindow connectWindwow = new ConnectWindow();
            connectWindwow.Show();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.Numerics;
using System.Runtime.InteropServices;

namespace SSOI___projekt
{
    /// <summary>
    /// Interaction logic for DivideSecretWindow.xaml
    /// </summary>
    /// 
    public partial class DivideSecretWindow : Window
    {
        /*
        private BigInteger secret;
        private BigInteger p;

        private List<BigInteger> shadows;
         *  */
        private Random rand = new Random();
        private int trehold_value;
        private int number_of_shadows;

        private byte[] secret;
        private int p = 257;

        List<List<byte>> shadows;
        //List<byte> x;

        public DivideSecretWindow()
        {
            InitializeComponent();
        }
                
        private void Select_File_Button_Click(object sender, RoutedEventArgs e)
        {
            secret = null;

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Otwórz plik";
            if (openFileDialog.ShowDialog() == true)
            {
                string file = openFileDialog.FileName;
                if (System.IO.File.Exists(file) == true)
                {
                    secret = System.IO.File.ReadAllBytes(file);
                    Select_File_TextBox.Text = file;
                }
                else
                    MessageBox.Show("Błąd", "Nie ma takiego pliku");

            }
            StringBuilder str = new StringBuilder();
            foreach (byte by in secret)
                str.Append(by.ToString() + " ");
            Secret_TextBox.Text = str.ToString();
        }

        private void Divide_Button_Click(object sender, RoutedEventArgs e)
        {
            if (!int.TryParse(Number_Of_Shadows_TextBox.Text, out number_of_shadows))
                Console.WriteLine("Błąd int.TryParse()");
            if (!int.TryParse(Treshold_TextBox.Text, out trehold_value))
                Console.WriteLine("Błąd int.TryParse()");

            
            List<byte> wspolczynnik = new List<byte>();
            for (int i = 0; i <= (trehold_value - 1); i++)
               wspolczynnik.Add((byte)rand.Next(1, 255));
            
            
            shadows = new List<List<byte>>();
            for (int i = 0; i < number_of_shadows; i++)
                shadows.Add(new List<byte>());

            int[] coef = new int[trehold_value];
            Random random = new Random();
            for (int i = 1; i < trehold_value; i++)
            {
                coef[i] = random.Next(0, p - 1);
            }
            StringBuilder str = new StringBuilder();
            for (int b = 0; b < secret.Length; b++)
            {
                str.Append("Sekret  " + (b + 1).ToString() + "\n");
                byte tempSecret = secret[b];
                for (int i = 0; i < number_of_shadows; i++)
                {
                    int value = 0;
                    str.Append("f(" + (i + 1).ToString() + ") = ");

                    for (int j = 0; j < (trehold_value - 1); j++)
                    {
                        //str.Append(wspolczynnik[j].ToString() + " * " + (i + 1).ToString() + "^" + (j + 1).ToString() + " + ");
                        value += (wspolczynnik[j] * (int)Math.Pow(i + 1, j + 1) % p) % p;
                    }
                    //str.Append(tempSecret.ToString() + " (mod 257) = ");
                    value += tempSecret;
                    str.Append(((byte)(value % p)).ToString() + "\n");

                    shadows[i].Add((byte)(value % p));
                }
                str.Append("\n");
            }
            
            
            foreach (List<byte> li in shadows)
            {
                byte[] aaa = li.ToArray();
                foreach (byte by in aaa)
                    str.Append(by.ToString() + " ");
                str.Append("\n");
            }

            Shadows_TextBox.Text = str.ToString();
        }

        private void Rec_Secret_Button_Click(object sender, RoutedEventArgs e)
        {
            
            //losowe cienie
            Random rnd = new Random();
            int[] index = new int[trehold_value];
            for (int i = 0; i < trehold_value; i++)
            {
                Flag:
                    int idx = rnd.Next(0, number_of_shadows);
                    for (int j = 0; j < i; j++)
                        if (index[j] == idx)
                            goto Flag;
                index[i] = idx;
            }
           
            StringBuilder str = new StringBuilder();
            str.Append("Losowe indeksy \n");
            for (int i = 0; i < index.Length; i++)
                str.Append((index[i] + 1).ToString() + " ");
            str.Append("\n\n\n");

            List<byte> rec = new List<byte>();
            for (int bi = 0; bi < secret.Length; bi++)
            {
                str.Append("Odzyskiwanie " + (bi + 1).ToString() + " bajta\n");
                
                int accum = 0;
                for (int i = 0; i < trehold_value; i++)
                {
                    int multiply = 1;
                    for (int j = 0; j < trehold_value; j++)
                    {
                        if (i == j)
                            continue;
                        
                        int up = (index[j] + 1) * -1;
                        int down = (index[i] + 1) - (index[j] + 1);

                        if (up < 0)
                            up = p + up;
                        if (down < 0)
                            down = p + down;

                        int modDown = modInverse(down, p);

                        //str.Append("Up   0 - " + (index[j] + 1) + " = " + up + "\n");
                        //str.Append("Down " + (index[i] + 1) + " - " + (index[j] + 1) + " = " + down + "\t" + down + "^-1 = " + modDown + "\n");
                        int mul = (up * modDown) % p;
                        //str.Append(up + " * " + modDown + " = " + mul + "\n");
                        //str.Append("Mnożenie: " + multiply + " * " + mul + " = ");
                        multiply = (multiply * mul) % p;
                        //str.Append(multiply + "\n" + "-------------------------------------------" + "\n");
                    }
                    int shadow = shadows[index[i]][bi];
                    //str.Append("Suma: " + accum + " + " + shadow + " * " + multiply + " = ");
                    accum = (accum + (shadow * multiply) % p) % p;
                    accum = (accum + p) % p;
                    //str.Append(accum + "\n" + "-----------------------------------------------------------------" + "\n");
                }
                str.Append("Wartość odzyskana: " + accum + "\n\n");
            }
            
            Rec_TextBox.Text = str.ToString();
            
        }

        int modInverse(int a, int n)
        {
            int copy = a;
            
            int i = n, v = 0, d = 1;
            while (a > 0)
            {
                int t = i / a, x = a;
                a = i % x;
                i = x;
                x = d;
                d = v - t * x;
                v = x;
            }
            v %= n;
            if (v < 0) v = (v + n) % n;
            //Console.WriteLine(copy + " * " + v + " mod " + n + " = 1");  
            //if (copy < 0)
            //    return v * -1;
            return v;
        }

        private void Copy_Shadows_Button_Click(object sender, RoutedEventArgs e)
        {
            /* Wersja dla wszystkich cieni
            for (int i = 0; i < number_of_shadows; i++)
            {
                CopyToCardWindow window = new CopyToCardWindow(number_of_shadows, trehold_value, i + 1, shadows[i].ToArray());
                window.Show();
                /* Czekanie na zakończenie kopiowania do karty
                 * Dorobić np. czekanie na wątek lub uspić wątek na jakiś czas,
                 * aby nie marnować zasobów procesora
                while (!window.ifCompleted) { }
                window.Close();
            }
            */

            /*
             * Wersja dla jednej karty,
             * aby sprawdzić, czy w ogóle działa kopiowanie
            */
            CopyToCardWindow window = new CopyToCardWindow(number_of_shadows, trehold_value, shadows);
            window.Show();
            //while (!window.ifCompleted) { }
            //window.Close();
        }
    }
}

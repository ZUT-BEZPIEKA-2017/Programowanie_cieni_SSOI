﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.Numerics;

namespace SSOI___projekt
{
    /// <summary>
    /// Interaction logic for DivideSecretWindow.xaml
    /// </summary>
    public partial class DivideSecretWindow : Window
    {
        /*
        private BigInteger secret;
        private BigInteger p;

        private List<BigInteger> shadows;
         *  */
        private Random rand = new Random();
        private int trehold_value;
        private int number_of_shadows;

        private byte[] secret;
        private int p = 257;

         List<List<byte>> shadows;

        public DivideSecretWindow()
        {
            InitializeComponent();

            //shadows = new List<byte[]>();
        }

        private void Select_File_Button_Click(object sender, RoutedEventArgs e)
        {
            secret = null;

            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Title = "Otwórz plik";
            if (openFileDialog.ShowDialog() == true)
            {
                string file = openFileDialog.FileName;
                if (System.IO.File.Exists(file) == true)
                {
                    secret = System.IO.File.ReadAllBytes(file);
                    Select_File_TextBox.Text = file;
                }
                else
                    MessageBox.Show("Błąd", "Nie ma takiego pliku");

            }
            StringBuilder str = new StringBuilder();
            foreach (byte by in secret)
                str.Append(by.ToString() + " ");
            Secret_TextBox.Text = str.ToString();
        }

        private void Divide_Button_Click(object sender, RoutedEventArgs e)
        {
            if (!int.TryParse(Number_Of_Shadows_TextBox.Text, out number_of_shadows))
                Console.WriteLine("Błąd int.TryParse()");
            if (!int.TryParse(Treshold_TextBox.Text, out trehold_value))
                Console.WriteLine("Błąd int.TryParse()");

            List<byte> wspolczynnikiA = new List<byte>();
            for (int i = 0; i < (trehold_value - 1); i++)
                wspolczynnikiA.Add((byte)rand.Next(0, 255));

            Console.WriteLine("p " + p);

            shadows = new List<List<byte>>();
            for (int i = 0; i < number_of_shadows; i++)
                shadows.Add(new List<byte>());

            for (int b = 0; b < secret.Length; b++)
            {
                byte tempSecret = secret[b];
                
                for (int i = 0; i < number_of_shadows; i++)
                {
                    byte value = 0;
                    for (int j = (wspolczynnikiA.Count - 1); j >= 0; j--)
                    {
                        value += (byte)(wspolczynnikiA[j] * Math.Pow(i + 1, j + 1));
                        //Console.WriteLine("value " + value + " wspolA " + wspolczynnikiA[j] + " pow " + Math.Pow(i + 1, j + 1));
                    }

                    value += tempSecret;
                    shadows[i].Add(value);
                }
            }

            StringBuilder str = new StringBuilder();
            foreach (List<byte> li in shadows)
            {
                byte[] aaa = li.ToArray();
                foreach (byte by in aaa)
                    str.Append(by.ToString() + " ");
                str.Append("\n");
            }

            Shadows_TextBox.Text = str.ToString();
        }

        private void Rec_Secret_Button_Click(object sender, RoutedEventArgs e)
        {
            
            //losowe cienie
            
            Random rnd = new Random();
            int[] index = new int[trehold_value];
            for (int i = 0; i < trehold_value; i++)
            {
                Flag:
                    int idx = rnd.Next(0, number_of_shadows);
                    for (int j = 0; j < i; j++)
                        if (index[j] == idx)
                            goto Flag;
                index[i] = idx;
            }
            
            //int[] index = new int[] { 1, 3, 4, 5, 7, 8, 9, 10, 11, 12 };
            StringBuilder str = new StringBuilder();
            str.Append("Losowe indeksy \n");
            for (int i = 0; i < index.Length; i++)
                str.Append((index[i] + 1).ToString() + " ");
            str.Append("\n");

            //próba
            List<byte> rec = new List<byte>();
            for (int b = 0; b < secret.Length; b++)
            {
                str.Append((b + 1).ToString() + "\n");
                //byte suma = 0;
                int firstMultiply = 1;
                double firstSum = 0.0;

                int suma = 0;
                for (int i = 0; i < trehold_value; i++)
                {
                    
                    //byte shadow
                    int shadow = shadows[index[i]][b];
                    str.Append("Cien " + shadow.ToString() + " ");

                    firstMultiply *= (-1 * index[i]);

                    int secondMultiply = 1;
                    for (int j = 0; j < trehold_value; j++)
                    {
                        if (index[i] != index[j])
                        {
                            secondMultiply *= (index[i] - index[j]);
                        }
                    }
                    firstSum += shadow / (-1.0 * index[i] * secondMultiply);
                }
                str.Append("firstSum " + firstSum.ToString() + "\n");
                str.Append("firstMultiply " + firstMultiply.ToString() + "\n");
                double sum = firstSum * firstMultiply;
                str.Append("Sum " + sum.ToString() + "\n");

                int val = (int)sum % p;
                str.Append("Wart " + val.ToString() + "\n");
                rec.Add((byte)(val));
                
            }
            /*
            List<byte> rec = new List<byte>();
            for (int b = 0; b < secret.Length; b++)
            {
                str.Append((b + 1).ToString() + "\n");
                //byte suma = 0;
                int suma = 0;
                for (int i = 0; i < trehold_value; i++)
                {
                    //byte shadow
                    int shadow = shadows[index[i]][b];
                    str.Append("Cien " + shadow.ToString() + " ");
                    double val = 1.0;
                    for (int j = 0; j < trehold_value; j++)
                    {
                        if (index[i] != index[j])
                        {
                            double tempVal = (0.0 - (index[j] + 1.0)) / ((index[i] + 1.0) - (index[j] + 1.0));
                            val = val * tempVal;
                        }
                    }
                    str.Append("Val " + val.ToString() + " ");
                    int mul = (int)(shadow * val);
                    str.Append("Mul " + mul.ToString() + " ");
                    suma += mul;
                    str.Append("Suma " + suma.ToString() + "\n");
                }
                //sprawdzić, co robić przy ujemnych wartościach
                if (suma < 0)
                {
                    str.Append("Wartosc ujemna \n");
                    suma += p * 4;
                }
                str.Append("Suma " + suma.ToString() + " ");
                byte wart = (byte)(suma % p);
                str.Append("Wart " + wart.ToString() + "\n");
                rec.Add((byte)(wart));
            }*/
            Rec_TextBox.Text = str.ToString();

            /*
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Title = "Zapisz plik";
            //saveFileDialog.Filter = "Tekst jawny (*.src)|*.src|Tekst jawny oczyszczony (*.clr)|*.clr|Tekst zaszyfrowany (*.enc)|*.enc|Tekst odszyfrowany (*.dec)|*.dec";
            saveFileDialog.ShowDialog();

            if (saveFileDialog.FileName != "")
            {
                //string text = Encoding.ASCII.GetString(bytesToSave);
                //System.IO.File.WriteAllText(saveFileDialog.FileName, text);
                System.IO.File.WriteAllBytes(saveFileDialog.FileName, rec.ToArray());

            }
            */
            /*
            StringBuilder str = new StringBuilder();
            foreach (byte by in rec)
                str.Append(by.ToString() + " ");
           
            /*
            BigInteger suma = new BigInteger();
            for (int i = 0; i < trehold_value; i++)
            {
                BigInteger shadow = shadows[index[i]];
                str.Append("Cien " + shadow + " ");
                
                double val = 1.0;
                for (int j = 0; j < trehold_value; j++)
                {
                    if (index[i] != index[j])
                    {
                        double tempVal = (0.0 - (index[j] + 1.0)) / ((index[i] + 1.0) - (index[j] + 1.0));
                        val = val * tempVal;
                    }
                }
                str.Append("Val " + val.ToString() + " ");
                BigInteger mul = BigInteger.Multiply(shadow, new BigInteger(val * 1000000000));
                mul = BigInteger.Divide(mul, 1000000000);
                str.Append("Multiply " + mul + " ");
                suma = BigInteger.Add(suma, mul);
                str.Append("Suma " + suma + "\n");
            }
            if (suma.Sign == -1)
            {
                suma = BigInteger.Add(suma, BigInteger.Multiply(p, 4));
                str.Append("Wartosc ujemna\n");
            }
            str.Append("\n Suma " + suma + "\n");
            BigInteger retVal = suma % p;
            str.Append("Suma % p " + retVal + "\n");
            byte[] bytes = retVal.ToByteArray();
            str.Append("tablica bajtów ");
            foreach (byte by in bytes)
                str.Append(by.ToString() + " ");

            
            //return suma % p;
             * */
        }
        /*
        public List<BigInteger> getShadows()
        {
            return shadows;
        }

        public int getTreholdValue()
        {
            return trehold_value;
        }

        public int getNumberOfShadows()
        {
            return number_of_shadows;
        }
         * */
    }
}

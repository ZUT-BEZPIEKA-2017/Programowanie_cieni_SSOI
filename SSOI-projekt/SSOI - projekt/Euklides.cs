﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSOI___projekt
{
    class Euklides
    {
        public static int euklides(int nwd_a, int nwd_b)
        {
            if (nwd_b > nwd_a)
                throw new Exception();

            int a = nwd_a;
            int b = nwd_b;

            int q = a / b;
            int r = a - q * b;
            int nwd = b;

            int x2 = 1;
            int x1 = 0;
            int y2 = 0;
            int y1 = 1;
            int x = 1;
            int y = y2 - (q - 1) * y1;

            while (r != 0)
            {
                a = b;
                b = r;

                x = x2 - q * x1;
                x2 = x1;
                x1 = x;

                y = y2 - q * y1;
                y2 = y1;
                y1 = y;

                nwd = r;
                q = a / b;
                r = a - q * b;
            }

            Console.WriteLine("NWD("+nwd_a+", "+nwd_b+") = "+nwd+" = "+x+" * "+nwd_a+" + "+y+" * "+nwd_b);
 
            if (nwd == 1)
                Console.WriteLine(nwd_b+" * "+y+" mod "+nwd_a+" = 1");

            return y;
        }
    }
}

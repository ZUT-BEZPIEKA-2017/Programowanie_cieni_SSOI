﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using Microsoft.Win32;
using System.Drawing;

using GS.PCSC;
using GS.SCard;
using GS.SCard.Const;

namespace SSOI___projekt
{
    /// <summary>
    /// Interaction logic for RecoveryWindow.xaml
    /// </summary>
    public partial class RecoveryWindow : Window
    {
        private WinSCard scard;
        private string readerName;
        private List<int> index;
        private List<byte> recovery;
        private List<List<byte>> shadows;
        private int numberOfShadows = 0;
        private int thresholdValue = 0;
        private int numberOfCards = 0;
        private int numberOfRecords = 0;
        private int p = 257;

        public RecoveryWindow()
        {
            InitializeComponent();
            RecoveryButton.IsEnabled = false;

            index = new List<int>();
            shadows = new List<List<byte>>();

            NumberOfCardsTextBox.Text = "0";
            NumberOfShadowsTextBox.Text = "0";
            ThresoldValueTextBox.Text = "0";

            ListReaders();
        }

        private void Select_Reader_ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                readerName = scard.ReaderNames[Select_Reader_ComboBox.SelectedIndex];
            }
            catch (Exception ex) { }
        }

        private void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //Połączenie z kartą
                scard.WaitForCardPresent(readerName);
                MessageBox.Show("Wykryto kartę");
                scard.Connect(readerName);
                MessageBox.Show("Rozpoczęto kopiowanie cieni z karty");

                //Wyswietlenie ATR
                Console.WriteLine("ATR: " + ByteToHex(scard.Atr));

                //Wybranie MF
                Console.WriteLine("\nWybieranie pliku MF");
                byte[] cmdApdu = { 0x00, 0xA4, 0x00, 0x0C, 0x02, 0x3F, 0x00 };
                byte[] respApdu = new byte[256];
                int respLength = respApdu.Length;
                scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);
                if (respApdu[0] != 0x90 || respApdu[1] != 0x00)
                {
                    Console.WriteLine("Response " + ByteToHex(respApdu));
                    MessageBox.Show("Niepowodzenie odczytu z karty Nr: " + (numberOfCards + 1), "Błąd");
                    ListReaders();
                    return;
                }
                else
                    Console.WriteLine("Powodzenie");

                //Wybranie DF
                Console.WriteLine("\nWybranie pliku DF 000A");
                cmdApdu = new byte[] { 0x00, 0xA4, 0x01, 0x0C, 0x02, 0x00, 0x0A };
                respApdu = new byte[256];
                respLength = respApdu.Length;
                scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);
                if (respApdu[0] != 0x90 || respApdu[1] != 0x00)
                {
                    Console.WriteLine("Response " + ByteToHex(respApdu));
                    MessageBox.Show("Niepowodzenie odczytu z karty Nr: " + (numberOfCards + 1), "Błąd");
                    ListReaders();
                    return;
                }
                else
                    Console.WriteLine("Powodzenie");


                //Weryfikacja PIN nr 1
                //Dorobić okno wprowadzenia PINu
                //Teraz jest na sztywno
                Console.WriteLine("\nUwierzytelnienie PINem Nr 1");
                string input = Microsoft.VisualBasic.Interaction.InputBox("Podaj PIN", "PIN: ", "");
                byte[] pin = Encoding.ASCII.GetBytes(input);
                List<byte> apduList = new List<byte>();
                apduList.Add(0x00);
                apduList.Add(0x20);
                apduList.Add(0x00);
                apduList.Add(0x81);
                //apduList.Add(0x08);
                apduList.Add((byte)pin.Length);
                apduList.AddRange(pin);
                cmdApdu = apduList.ToArray();
                //cmdApdu = new byte[] { 0x00, 0x20, 0x00, 0x81, 0x08, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31, 0x31 };
                respApdu = new byte[256];
                respLength = respApdu.Length;
                scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);
                if (respApdu[0] != 0x90 || respApdu[1] != 0x00)
                {
                    Console.WriteLine("Response " + ByteToHex(respApdu));
                    MessageBox.Show("Niepowodzenie odczytu z karty Nr: " + (numberOfCards + 1), "Błąd");
                    ListReaders();
                    return;
                }
                else
                    Console.WriteLine("Powodzenie");

                //Wybór pliku transparentnego EF001A
                Console.WriteLine("\nWybór pliku EF001A");
                cmdApdu = new byte[] { 0x00, 0xA4, 0x02, 0x0C, 0x02, 0x00, 0x1A };
                respApdu = new byte[256];
                respLength = respApdu.Length;
                scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);
                if (respApdu[0] != 0x90 || respApdu[1] != 0x00)
                {
                    Console.WriteLine("Response " + ByteToHex(respApdu));
                    MessageBox.Show("Niepowodzenie odczytu z karty Nr: " + (numberOfCards + 1), "Błąd");
                    ListReaders();
                    return;
                }
                else
                    Console.WriteLine("Powodzenie");
                
                //Odczyt z pliku transparentnego EF001A
                Console.WriteLine("\nOdczyt z pliku transparentnego");
                cmdApdu = new byte[] { 0x00, 0xB0, 0x00, 0x00, 0x04 };
                respApdu = new byte[256];
                respLength = respApdu.Length;
                scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);
                if (respApdu[4] != 0x90 || respApdu[5] != 0x00)
                {
                    Console.WriteLine("Response " + ByteToHex(respApdu));
                    MessageBox.Show("Niepowodzenie odczytu z karty Nr: " + (numberOfCards + 1), "Błąd");
                    ListReaders();
                    return;
                }
                else
                    Console.WriteLine("Powodzenie");
                //Pobranie ilości cieni, wartści progowej, x i ilości rekordów w pliku EF01B
                //Ilosc cieni
                if (numberOfShadows == 0)
                {
                    numberOfShadows = respApdu[0];
                    //for (int i = 0; i < numberOfShadows; i++)
                    //    shadows.Add(new List<byte>());
                    NumberOfShadowsTextBox.Text = numberOfShadows.ToString();
                }
                else if (numberOfShadows != respApdu[0])
                {
                    //Wartości z kart sie roznia
                    Console.WriteLine("Response " + ByteToHex(respApdu));
                    MessageBox.Show("Niepowodzenie odczytu z karty Nr: " + (numberOfCards + 1), "Błąd");
                    ListReaders();
                    return;
                }
                //Wartosc progowa
                if (thresholdValue == 0)
                {
                    thresholdValue = respApdu[1];
                    ThresoldValueTextBox.Text = thresholdValue.ToString();
                }
                else if (thresholdValue != respApdu[1])
                {
                    //Wartości z kart sie roznia
                    Console.WriteLine("Response " + ByteToHex(respApdu));
                    MessageBox.Show("Niepowodzenie odczytu z karty Nr: " + (numberOfCards + 1), "Błąd");
                    ListReaders();
                    return;
                }
                int currentX = 0;
                //X
                if (respApdu[2] != 0x00)
                {
                    index.Add((int)respApdu[2]);
                    currentX = (int)respApdu[2];
                }
                //Ilosc rekordow
                if (respApdu[3] != 0x00)
                    numberOfRecords = (int)respApdu[3];

                //Uwierzytelnienie PINem Nr 2
                Console.WriteLine("\nUwierzytelnienie PINem Nr 2");
                input = Microsoft.VisualBasic.Interaction.InputBox("Podaj PIN", "PIN: ", "");
                pin = Encoding.ASCII.GetBytes(input);
                apduList = new List<byte>();
                apduList.Add(0x00);
                apduList.Add(0x20);
                apduList.Add(0x00);
                apduList.Add(0x82);
                //apduList.Add(0x08);
                apduList.Add((byte)pin.Length);
                apduList.AddRange(pin);
                cmdApdu = apduList.ToArray();
                //cmdApdu = new byte[] { 0x00, 0x20, 0x00, 0x82, 0x08, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11 };
                respApdu = new byte[256];
                respLength = respApdu.Length;
                scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);
                if (respApdu[0] != 0x90 || respApdu[1] != 0x00)
                {
                    Console.WriteLine("Response " + ByteToHex(respApdu));
                    MessageBox.Show("Niepowodzenie odczytu z karty Nr: " + (numberOfCards + 1), "Błąd");
                    ListReaders();
                    return;
                }
                else
                    Console.WriteLine("Powodzenie");

                //Wybór pliku EF001B
                Console.WriteLine("\nWybór pliku EF001B");
                cmdApdu = new byte[] { 0x00, 0xA4, 0x02, 0x0C, 0x02, 0x00, 0x1B };
                respApdu = new byte[256];
                respLength = respApdu.Length;
                scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);
                if (respApdu[0] != 0x90 || respApdu[1] != 0x00)
                {
                    Console.WriteLine("Response " + ByteToHex(respApdu));
                    MessageBox.Show("Niepowodzenie odczytu z karty Nr: " + (numberOfCards + 1), "Błąd");
                    ListReaders();
                    return;
                }
                else
                    Console.WriteLine("Powodzenie");

                /*
                 * Dorobić odczyt rekordów EF001B
                 * zapisać wszystkie bajty do listy
                 * zapisane dodawać do listy shadows
                 * shadows[numerkarty].Add(new List<byte>());
                 * jak się uda zwiększyć numberOfCards
                 * jak numberOfards będzie równe thresholdValue
                 * odblokować przycisk Recovery
                 * od nowa zainicjować wybranie czytnika ListReaders()
                 * dorobic obsługę wyjątków i zakończenie działąnia po wykryciu błędów
                 */

                //Lista bajtów skopiowanych z karty
                List<byte> bytes = new List<byte>();                    
                for (int i = 1; i <= numberOfRecords; i++)
                {
                    apduList = new List<byte>();
                    apduList.Add(0x00);
                    apduList.Add(0xB2);
                    apduList.Add((byte)i);
                    apduList.Add(0x04);
                    apduList.Add(0x00);
                    cmdApdu = apduList.ToArray();
                    respApdu = new byte[256];
                    respLength = respApdu.Length;
                    scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);

                    /*
                     * option:
                     * 0 - błąd
                     * 1 - cały rekord zapisany
                     * 2 - nie cały
                    */ 
                    int option = 0;
                    
                    /* Sprawdzenie, czy czytanie rekordu zakończyło się powodzeniem
                     * i czy jest wypełniony cały rekord
                    */
                    if (respApdu[respLength - 2] == 0x90 && respApdu[respLength - 1] == 0x00)
                        option = 1;

                    /* Poszukiwanie response 0x90 0x00
                    */
                    int responseEnd = 0; //Indeks końca ramki odpowiedzi (reszta same zera)
                    for (int j = respLength - 1; j > 1; j--)
                    {
                        if (respApdu[j] == 0x00 && respApdu[j - 1] == 0x90)
                        {
                            option = 2;
                            responseEnd = j;
                            break;
                        }
                    }

                    switch (option)
                    {
                        case 0:
                            MessageBox.Show("Niepowodzenie odczytu z karty Nr: " + (numberOfCards + 1), "Błąd");
                            ListReaders();
                            break;
                        case 1:
                            for (int j = 0; j < (respLength - 2); j++)
                                bytes.Add(respApdu[j]);
                            break;
                        case 2:
                            for (int j = 0; j < (responseEnd - 1); j++)
                                bytes.Add(respApdu[j]);
                            break;
                    }
                }
                shadows.Add(bytes);
                numberOfCards++;
                NumberOfCardsTextBox.Text = numberOfCards.ToString();
                ListReaders();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Niepowodzenie odczytu z karty Nr: " + (numberOfCards + 1) + ex.ToString(), "Błąd");
                ListReaders();
            }
        }

        private void RecoveryButton_Click(object sender, RoutedEventArgs e)
        {
            //Odzyskane sekrety
            recovery = new List<byte>();
            
            StringBuilder str = new StringBuilder();
            for (int bi = 0; bi < shadows[0].Count; bi++)
            {
                str.Append("Odzyskiwanie " + (bi + 1).ToString() + " bajta\n");

                int accum = 0;
                for (int i = 0; i < thresholdValue; i++)
                {
                    int multiply = 1;
                    for (int j = 0; j < thresholdValue; j++)
                    {
                        if (i == j)
                            continue;

                        int up = (index[j]) * -1;
                        int down = (index[i]) - (index[j]);

                        if (up < 0)
                            up = p + up;
                        if (down < 0)
                            down = p + down;

                        int modDown = modInverse(down, p);

                        int mul = (up * modDown) % p;
                        multiply = (multiply * mul) % p;
                    }
                    int shadow = shadows[i][bi];
                    //try
                    //{
                        
                    //}
                    //catch (Exception ex) { }
                    accum = (accum + (shadow * multiply) % p) % p;
                    accum = (accum + p) % p;
                }
                recovery.Add((byte)accum);
                str.Append("Wartość odzyskana: " + accum + "\n\n");
            }
            MainTextBox.Text = str.ToString();
            SavePicturePNG.IsEnabled = true;
            SavePictureJPG.IsEnabled = true;
            SaveTxt.IsEnabled = true;
        }

        int modInverse(int a, int n)
        {
            int copy = a;

            int i = n, v = 0, d = 1;
            while (a > 0)
            {
                int t = i / a, x = a;
                a = i % x;
                i = x;
                x = d;
                d = v - t * x;
                v = x;
            }
            v %= n;
            if (v < 0) v = (v + n) % n;
            return v;
        }

        public string ByteToHex(byte[] array)
        {
            string hex = BitConverter.ToString(array).Replace("-", " ");
            return hex;
        }

        private void ListReaders()
        {
            if ((numberOfCards != 0) && (numberOfCards == thresholdValue))
                RecoveryButton.IsEnabled = true;

            Select_Reader_ComboBox.Items.Clear();
            scard = new WinSCard();
            try
            {
                scard.EstablishContext();
                scard.ListReaders();

                //Console.WriteLine("Available PC/SC Readers:\n");
                for (int i = 0; i < scard.ReaderNames.Length; i++)
                {
                    Select_Reader_ComboBox.Items.Add(scard.ReaderNames[i]);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
        }

        private void SaveTxt_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Title = "Zapisz plik";
            saveFileDialog.Filter = "Plik tekstowy (*.txt)|*.txt";
            saveFileDialog.ShowDialog();

            if (saveFileDialog.FileName != "")
            {
                System.IO.File.WriteAllBytes(saveFileDialog.FileName, recovery.ToArray());
            }
        }

        private void SavePictureJPG_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Title = "Zapisz plik";
            saveFileDialog.Filter = "Plik JPG (*.jpg)|*.jpg";
            saveFileDialog.ShowDialog();

            if (saveFileDialog.FileName != "")
            {
                
            }
        }

        private void SavePicturePNG_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Title = "Zapisz plik";
            saveFileDialog.Filter = "Plik PNG (*.png)|*.png";
            saveFileDialog.ShowDialog();

            if (saveFileDialog.FileName != "")
            {
                System.IO.File.WriteAllBytes(saveFileDialog.FileName, recovery.ToArray());
            }
        }
    }
}

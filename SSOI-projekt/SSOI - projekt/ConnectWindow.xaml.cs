﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using GS.PCSC;
using GS.SCard;
using GS.SCard.Const;

namespace SSOI___projekt
{
    /// <summary>
    /// Interaction logic for ConnectWindow.xaml
    /// </summary>
    public partial class ConnectWindow : Window
    {
        WinSCard scard;
        string readerName;

        public ConnectWindow()
        {
            InitializeComponent();

            scard = new WinSCard();

            try
            {
                scard.EstablishContext();
                scard.ListReaders();

                //Console.WriteLine("Available PC/SC Readers:\n");
                for (int i = 0; i < scard.ReaderNames.Length; i++)
                {
                    Select_Reader_ComboBox.Items.Add(scard.ReaderNames[i]);
                    //Console.WriteLine(string.Format("Reader {0:#0}: {1}", i, scard.ReaderNames[i]));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
                //this.Close();
            }
            
        }

        private void Select_Reader_ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            readerName = scard.ReaderNames[Select_Reader_ComboBox.SelectedIndex]; 
        }

        private void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                scard.WaitForCardPresent(readerName);
                MessageBox.Show("Wykryto kartę");
                scard.Connect(readerName);
                MessageBox.Show("Połączono z kartą");

                Console.WriteLine("ATR: 0x" + scard.AtrString);

                byte[] cmdApdu = { 0xFF, 0xCA, 0x00, 0x00, 00 };
                byte[] respApdu = new byte[256];
                int respLength = respApdu.Length;
                scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);

                Console.WriteLine("Command APDU ");
                foreach (byte by in cmdApdu)
                    Console.Write(by.ToString() + " ");
                Console.Write("\n");
                Console.WriteLine("Response APDU ");
                foreach (byte by in respApdu)
                    Console.Write(by.ToString() + " ");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd");
                return;
            }
        }

        private void SelectMFButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Console.WriteLine("\nWybieranie pliku MF");
                byte[] cmdApdu = { 0x00, 0xA4, 0x00, 0x0C, 0x02, 0x3F, 0x00 }; 
                byte[] respApdu = new byte[256];
                int respLength = respApdu.Length;
                scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);

                Console.WriteLine("Command APDU ");
                foreach (byte by in cmdApdu)
                    Console.Write(by.ToString() + " ");
                Console.Write("\n");
                Console.WriteLine("Response APDU ");
                foreach (byte by in respApdu)
                    Console.Write(by.ToString() + " ");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd");
                return;
            }
        }

        private void CreateDFButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Console.WriteLine("\nTworzenie pliku DF");

                //Zmienić fid df - dwa ostanie bajty
                byte[] cmdApdu = { 0x00, 0xE0, 0x38, 0x00, 0x06, 0x62, 0x04, 0x83, 0x02, 0x00, 0x00 };
                byte[] respApdu = new byte[256];
                int respLength = respApdu.Length;
                scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);

                Console.WriteLine("Command APDU ");
                foreach (byte by in cmdApdu)
                    Console.Write(by.ToString() + " ");
                Console.Write("\n");
                Console.WriteLine("Response APDU ");
                foreach (byte by in respApdu)
                    Console.Write(by.ToString() + " ");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd");
                return;
            }
        }

        private void SelectDFButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Console.WriteLine("\nWybranie pliku DF");

                //Zmienić fid df - dwa przedostanie bajty
                byte[] cmdApdu = { 0x00, 0xA4, 0x01, 0x0C, 0x02, 0x00, 0x00, 0x02};
                byte[] respApdu = new byte[256];
                int respLength = respApdu.Length;
                scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);

                Console.WriteLine("Command APDU ");
                foreach (byte by in cmdApdu)
                    Console.Write(by.ToString() + " ");
                Console.Write("\n");
                Console.WriteLine("Response APDU ");
                foreach (byte by in respApdu)
                    Console.Write(by.ToString() + " ");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd");
                return;
            }
        }

        private void CreateEFButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Console.WriteLine("\nTworzenie pliku EF - plik transparentny");

                //Zmienić fid df - dwa ostanie bajty
                byte[] cmdApdu = { 0x00, 0xE0, 0x00, 0x00, 0x06, 0x62, 0x04, 0x82, 0x01, 0x00, 0x00 };
                byte[] respApdu = new byte[256];
                int respLength = respApdu.Length;
                scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);

                Console.WriteLine("Command APDU ");
                foreach (byte by in cmdApdu)
                    Console.Write(by.ToString() + " ");
                Console.Write("\n");
                Console.WriteLine("Response APDU ");
                foreach (byte by in respApdu)
                    Console.Write(by.ToString() + " ");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd");
                return;
            }
        }

        private void SelectEFButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Console.WriteLine("\nWybieranie pliku EF");

                //Zmienić fid ef - 0x00, 0x00
                byte[] cmdApdu = { 0x00, 0xA4 , 0x02, 0x0C, 0x00, 0x00, 0x02};
                byte[] respApdu = new byte[256];
                int respLength = respApdu.Length;
                scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);

                Console.WriteLine("Command APDU ");
                foreach (byte by in cmdApdu)
                    Console.Write(by.ToString() + " ");
                Console.Write("\n");
                Console.WriteLine("Response APDU ");
                foreach (byte by in respApdu)
                    Console.Write(by.ToString() + " ");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd");
                return;
            }
        }

        private void WriteEFButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Console.WriteLine("\nZapis do pliku EF");

                byte[] cmdApdu = { 0x00, 0xD6, 0x00, 0x80, 0x04,  0xFF, 0xAA, 0xFF, 0x73 };
                byte[] respApdu = new byte[256];
                int respLength = respApdu.Length;
                scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);

                Console.WriteLine("Command APDU ");
                foreach (byte by in cmdApdu)
                    Console.Write(by.ToString() + " ");
                Console.Write("\n");
                Console.WriteLine("Response APDU ");
                foreach (byte by in respApdu)
                    Console.Write(by.ToString() + " ");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd");
                return;
            }
        }

        private void ReadEFButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Console.WriteLine("\nCzytanie pliku EF");

                byte[] cmdApdu = { 0x00, 0xB0,  0x00, 0x00, 0x00};
                byte[] respApdu = new byte[256];
                int respLength = respApdu.Length;
                scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);

                Console.WriteLine("Command APDU ");
                foreach (byte by in cmdApdu)
                    Console.Write(by.ToString() + " ");
                Console.Write("\n");
                Console.WriteLine("Response APDU ");
                foreach (byte by in respApdu)
                    Console.Write(by.ToString() + " ");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd");
                return;
            }
        }
        
        private void GenRSAButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Console.WriteLine("\nGenerowanie kluczy RSA");

                //Sprawdzić apdu
                byte[] cmdApdu = { 0x00, 0x46, 0x02, 0x00 };
                byte[] respApdu = new byte[256];
                int respLength = respApdu.Length;
                scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);

                Console.WriteLine("Command APDU ");
                foreach (byte by in cmdApdu)
                    Console.Write(by.ToString() + " ");
                Console.Write("\n");
                Console.WriteLine("Response APDU ");
                foreach (byte by in respApdu)
                    Console.Write(by.ToString() + " ");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd");
                return;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                scard.Disconnect();
                scard.ReleaseContext();
                Console.WriteLine("Zamykanie połączenia z kartą");
            }
            catch (Exception ex) { }
        }

        private void DeleteEFButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Console.WriteLine("\nUsuwanie pliku EF");

                //Zmienić fid ef - 0x00, 0x00
                byte[] cmdApdu = { 0x00, 0xE4, 0x02, 0x00, 0x02, 0x00, 0x00 };
                byte[] respApdu = new byte[256];
                int respLength = respApdu.Length;
                scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);

                Console.WriteLine("Command APDU ");
                foreach (byte by in cmdApdu)
                    Console.Write(by.ToString() + " ");
                Console.Write("\n");
                Console.WriteLine("Response APDU ");
                foreach (byte by in respApdu)
                    Console.Write(by.ToString() + " ");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd");
                return;
            }
        }

        private void DeleteDFButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Console.WriteLine("\nUsuwanie pliku DF");

                byte[] cmdApdu = { 0x00, 0xE4, 0x00, 0x00, 0x00 };
                byte[] respApdu = new byte[256];
                int respLength = respApdu.Length;
                scard.Transmit(cmdApdu, cmdApdu.Length, respApdu, ref respLength);

                Console.WriteLine("Command APDU ");
                foreach (byte by in cmdApdu)
                    Console.Write(by.ToString() + " ");
                Console.Write("\n");
                Console.WriteLine("Response APDU ");
                foreach (byte by in respApdu)
                    Console.Write(by.ToString() + " ");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd");
                return;
            }
        }
    }
}
